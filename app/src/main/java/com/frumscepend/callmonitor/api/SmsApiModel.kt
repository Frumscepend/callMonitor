package com.frumscepend.callmonitor.api

import com.frumscepend.callmonitor.data.SmsModel
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SmsApiModel{
    @SerializedName("key")
    @Expose
    var key: String? = null
    @SerializedName("act")
    @Expose
    var act: String? = "add"
    @SerializedName("item_id")
    @Expose
    var itemId:Int? = null
    @SerializedName("phone_number")
    @Expose
    var phoneNumber: String? = null
    @SerializedName("name")
    @Expose
    var name: String? = null
    @SerializedName("type")
    @Expose
    var type: Int? = null
    @SerializedName("date")
    @Expose
    var date: Long? = null
    @SerializedName("text")
    @Expose
    var text: String? = null
    @SerializedName("deleted_date")
    @Expose
    var deletedDate: Long? = null

    fun create(smsModel: SmsModel){
        phoneNumber = smsModel.phoneNumber
        name = smsModel.name
        type = smsModel.type
        date = smsModel.date
        text = smsModel.text
        deletedDate = smsModel.deletedDate
        itemId = smsModel.uid?.toInt()
    }
}