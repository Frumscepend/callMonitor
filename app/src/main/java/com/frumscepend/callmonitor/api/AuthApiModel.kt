package com.frumscepend.callmonitor.api

import android.arch.persistence.room.ColumnInfo
import com.frumscepend.callmonitor.data.SmsModel
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class AuthApiModel{
    @SerializedName("act")
    @Expose
    var act: String? = "auth"
    @SerializedName("login")
    @Expose
    var login: String? = null
    @SerializedName("password")
    @Expose
    var password: String? = null
}