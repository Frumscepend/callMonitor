package com.frumscepend.callmonitor.api

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UserApiModel{
    @SerializedName("user_id")
    @Expose
    var user_id: String? = null
    @SerializedName("key")
    @Expose
    var key: String? = null
}