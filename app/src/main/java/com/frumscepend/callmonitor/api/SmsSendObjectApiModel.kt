package com.frumscepend.callmonitor.api

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SmsSendObjectApiModel {

    @SerializedName("sms_count")
    @Expose
    var smsCount: Int? = null
    @SerializedName("sms")
    @Expose
    var sms: List<SmsSendApiModel>? = null

}