package com.frumscepend.callmonitor.api

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class CallSendApiModel {

    @SerializedName("callid")
    @Expose
    var callid: Int? = null
    @SerializedName("phone")
    @Expose
    var phone: String = ""

}