package com.frumscepend.callmonitor.api;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface RemoteApi {
    @POST("save_sms.php")
    Call<ResponseApiModel> saveSMS(@Body SmsApiModel smsApiModel);

    @POST("save_call.php")
    Call<ResponseApiModel> saveCall(@Body CallApiModel callApiModel);

    @Multipart
    @POST("save_call.php")
    Call<Void> saveCallAudio(@Query("item_id") String id, @Query("act") String act, @Query("key") String key, @Part MultipartBody.Part file);

    @POST("save_call.php")
    Call<UserApiModel> auth(@Body AuthApiModel authApiModel);

    @POST("get_sms.php")
    Call<SmsSendObjectApiModel> getSmsToSend(@Body SmsRequestApiModel smsRequestApiModel);

    @POST("get_sms.php")
    Call<Void> setSmsToSend(@Body SmsSetApiModel smsSetApiModel);

    @POST("get_sms.php")
    Call<CallSendObjectApiModel> getNumbersToCall(@Body SmsRequestApiModel smsRequestApiModel);

    @POST("get_call.php")
    Call<Void> setNumbersToCall(@Body CallSetApiModel callSetApiModel);

    @POST("save_call.php")
    Call<Void> setIncomigCallAlert(@Body IncomigCallAlert incomigCallAlert);
}
