package com.frumscepend.callmonitor.main.activity

import android.Manifest
import android.app.ActivityManager
import android.app.Dialog
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.CallLog
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.frumscepend.callmonitor.api.ApiClient
import com.frumscepend.callmonitor.api.AuthApiModel
import com.frumscepend.callmonitor.api.RemoteApi
import com.frumscepend.callmonitor.api.UserApiModel
import com.frumscepend.callmonitor.data.CallDataSource
import com.frumscepend.callmonitor.data.CallModel
import com.frumscepend.callmonitor.data.SmsDataSource
import com.frumscepend.callmonitor.data.SmsModel
import com.frumscepend.callmonitor.data.database.CallDatabaseDataSource
import com.frumscepend.callmonitor.data.database.MyDatabase
import com.frumscepend.callmonitor.data.database.SmsDatabaseDataSource
import com.frumscepend.callmonitor.main.service.ListenerService
import com.frumscepend.callmonitor.utils.AppExecutors
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import com.frumscepend.callmonitor.R


class MainActivity : AppCompatActivity() {
    private val REQUEST_READ_CALL_LOG = 90
    private val apiClient = ApiClient.getClient().create(RemoteApi::class.java)
    private lateinit var sharedPref: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        sharedPref = applicationContext.getSharedPreferences("userdetails", Service.MODE_PRIVATE)
        if (sharedPref.getLong("install_date", 0L) == 0L){
            sharedPref.edit().putLong("install_date", Date().time).apply()
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.PROCESS_OUTGOING_CALLS) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.READ_CALL_LOG,
                            Manifest.permission.READ_CONTACTS, Manifest.permission.READ_SMS,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_PHONE_STATE,
                            Manifest.permission.RECORD_AUDIO,
                            Manifest.permission.PROCESS_OUTGOING_CALLS,
                            Manifest.permission.SEND_SMS,
                            Manifest.permission.CALL_PHONE),
                    REQUEST_READ_CALL_LOG)
        } else {
            runListenerService()
        }
        findViewById<Button>(R.id.buttonUpdate).setOnClickListener {
            findViewById<TextView>(R.id.textViewLogSms).text = "Загружаю СМС"
            findViewById<TextView>(R.id.textViewLogCall).text = "Загружаю Звонки"
            CallDatabaseDataSource.getInstance(AppExecutors(), MyDatabase.getInstance(applicationContext).callsDao()).getCalls(object : CallDataSource.CallsCallBack {
                override fun onSuccess(calls: List<CallModel>) {
                    findViewById<TextView>(R.id.textViewLogCall).text = "Звонки:\n"
                    for (call in calls) {
                        var text = ""
                        text += "Номер: ${call.phoneNumber}\n"
                        text += "Имя: ${call.name}\n"
                        text += "Тип: ${when (call.type) {
                            CallLog.Calls.OUTGOING_TYPE -> "Исходящий"

                            CallLog.Calls.INCOMING_TYPE -> "Входящий"

                            CallLog.Calls.MISSED_TYPE -> "Пропущенный"
                            else -> {
                                ""
                            }
                        }}\n"
                        text += "Дата: ${SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss").format(Date(call.date))}\n"
                        text += "Длительность: ${call.duration}\n"
                        if (call.deleted) {
                            text += "Дата удаления: ${SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss").format(Date(call.deletedDate!!))}\n"
                        }
                        text += "============================\n"
                        findViewById<TextView>(R.id.textViewLogCall).text = findViewById<TextView>(R.id.textViewLogCall).text.toString() + text
                    }
                }

                override fun onFail() {
                    findViewById<TextView>(R.id.textViewLogCall).text = "Звонки:\nНе было звонков с момента установки приложения"
                }

            })
            SmsDatabaseDataSource.getInstance(AppExecutors(), MyDatabase.getInstance(applicationContext).smsDao()).getSmsList(object : SmsDataSource.SmsListCallBack{
                override fun onSuccess(smsList: List<SmsModel>) {
                    findViewById<TextView>(R.id.textViewLogSms).text = "СМС:\n"
                    for (sms in smsList) {
                        var text = ""
                        text += "Номер: ${sms.phoneNumber}\n"
                        text += "Имя: ${sms.name}\n"
                        text += "Тип: ${if(sms.type == 1) "Входящее" else "Исходящее"}\n"
                        text += "Дата: ${SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss").format(Date(sms.date))}\n"
                        text += "Текст: ${sms.text}\n"
                        if (sms.deleted) {
                            text += "Дата удаления: ${SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss").format(Date(sms.deletedDate!!))}\n"
                        }
                        text += "============================\n"
                        findViewById<TextView>(R.id.textViewLogSms).text = findViewById<TextView>(R.id.textViewLogSms).text.toString() + text
                    }
                }

                override fun onFail() {
                    findViewById<TextView>(R.id.textViewLogSms).text = "СМС:\nНе было смс с момента установки приложения"
                }

            })
        }
        findViewById<Button>(R.id.buttonAudio).setOnClickListener {
            startActivity(Intent(MainActivity@this, AudioActivity::class.java))
        }
        findViewById<Button>(R.id.buttonSettings).setOnClickListener {
            startActivity(Intent(MainActivity@this, SettingsActivity::class.java))
        }
    }

    private fun runListenerService(){
        val dialog = Dialog(this)
        dialog.setContentView(R.layout.dialog_auth)
        dialog.findViewById<EditText>(R.id.editTextLogin)?.setText("android_test")
        dialog.findViewById<EditText>(R.id.editTextPassword)?.setText("B9WgNdf7")
        dialog.findViewById<Button>(R.id.buttonLogin)?.setOnClickListener {
            val authApiModel = AuthApiModel()
            authApiModel.login = dialog.findViewById<EditText>(R.id.editTextLogin)?.text.toString()
            authApiModel.password = dialog.findViewById<EditText>(R.id.editTextPassword)?.text.toString()
            apiClient.auth(authApiModel).enqueue(object: Callback<UserApiModel> {
                override fun onFailure(call: Call<UserApiModel>?, t: Throwable?) {
                    Toast.makeText(this@MainActivity, "Ошибка сервера", Toast.LENGTH_SHORT).show()
                }

                override fun onResponse(call: Call<UserApiModel>?, response: Response<UserApiModel>?) {
                    if (response?.body()?.user_id?.toInt() == 0) {
                        Toast.makeText(this@MainActivity, "Неверный логин/пароль", Toast.LENGTH_SHORT).show()
                    } else {
                        dialog.dismiss()
                        sharedPref.edit().putString("key", response?.body()?.key!!).apply()
                        if (!isServiceRunning()) {
                            val serviceIntent = Intent(this@MainActivity, ListenerService::class.java)
                            startService(serviceIntent)
                        }
                    }
                }
            })
        }
        dialog.setCancelable(false)
        dialog.show()
    }

    private fun isServiceRunning(): Boolean {
        val manager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Integer.MAX_VALUE)) {
            if ("com.frumscepend.callmonitor.main.service.ListenerService" == service.service.className) {
                return true
            }
        }
        return false
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CALL_LOG) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            runListenerService()
        }
    }

}
