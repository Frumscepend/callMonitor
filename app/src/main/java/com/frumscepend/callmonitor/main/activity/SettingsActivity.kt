package com.frumscepend.callmonitor.main.activity

import android.app.Service
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import com.frumscepend.callmonitor.R

class SettingsActivity : AppCompatActivity() {

    private lateinit var sharedPref: SharedPreferences
    private lateinit var editTextCount: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        editTextCount = findViewById(R.id.editTextCount)

        sharedPref = applicationContext.getSharedPreferences("userdetails", Service.MODE_PRIVATE)
        editTextCount.setText(sharedPref.getInt("records_count", 1000).toString())

    }

    override fun onStop() {
        sharedPref.edit().putInt("records_count", editTextCount.text.toString().toInt()).apply()
        super.onStop()
    }
}
