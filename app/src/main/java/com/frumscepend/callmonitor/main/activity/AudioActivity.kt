package com.frumscepend.callmonitor.main.activity

import android.content.Context
import android.content.ContextWrapper
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ListView
import android.widget.TextView
import com.frumscepend.callmonitor.R
import com.frumscepend.callmonitor.main.adapter.SoundPlayerAdapter
import java.io.File

class AudioActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_audio)
        val cw = ContextWrapper(applicationContext)
        // path to /data/data/app/app_data
        val files = File(cw.getDir("records", Context.MODE_PRIVATE).path + "/sound").listFiles()
        val fileList = ArrayList<String>()
        if (files != null && files.isNotEmpty()) {
            for (file in files) {
                fileList.add(file.path)
            }
        }
        findViewById<ListView>(R.id.listViewAudio).adapter = SoundPlayerAdapter(fileList)
    }
}
