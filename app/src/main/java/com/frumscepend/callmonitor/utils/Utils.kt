package com.frumscepend.callmonitor.utils

import android.content.Context
import android.net.Uri
import android.provider.ContactsContract

class Utils{
    companion object {
        fun getContactName(phoneNumber: String, context: Context): String {
            val uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber))
            val projection = arrayOf(ContactsContract.PhoneLookup.DISPLAY_NAME)
            var contactName = ""
            val cursor = context.contentResolver.query(uri, projection, null, null, null)
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    contactName = cursor.getString(0)
                }
                cursor.close()
            }
            return contactName
        }
    }
}