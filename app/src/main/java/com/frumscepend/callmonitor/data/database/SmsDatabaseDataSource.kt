package com.frumscepend.callmonitor.data.database

import android.support.annotation.VisibleForTesting
import com.frumscepend.callmonitor.data.SmsDataSource
import com.frumscepend.callmonitor.data.SmsModel
import com.frumscepend.callmonitor.utils.AppExecutors
import java.util.*

class SmsDatabaseDataSource private constructor(
        private val appExecutors: AppExecutors,
        private val smsDao: SmsDao
): SmsDataSource{

    override fun getSmsList(callback: SmsDataSource.SmsListCallBack) {
        appExecutors.networkIO.execute {
            val smsList = smsDao.getAllSmsList()
            appExecutors.mainThread.execute {
                if (smsList.isEmpty()) {
                    callback.onFail()
                } else {
                    callback.onSuccess(smsList)
                }
            }
        }
    }

    override fun getSmsListFromDate(providedDate: Long, callback: SmsDataSource.SmsListCallBack) {
        appExecutors.networkIO.execute {
            val smsList = smsDao.getCallsFromDate(providedDate)
            appExecutors.mainThread.execute {
                if (smsList.isEmpty()) {
                    callback.onFail()
                } else {
                    callback.onSuccess(smsList)
                }
            }
        }
    }

    override fun updateSms(sms: SmsModel) {
        appExecutors.networkIO.execute {
            smsDao.updateSms(sms)
        }
    }

    override fun setSmsList(smsList: List<SmsModel>) {
        appExecutors.networkIO.execute {
            val prevSmsList = smsDao.getAllSmsList()
            val smsListResult = ArrayList<SmsModel>()
            for (prevSms in prevSmsList) {
                var found = false
                for (sms in smsList) {
                    if (sms == prevSms) {
                        found = true
                    }
                }
                if (!found && !prevSms.deleted) {
                    prevSms.deleted = true
                    prevSms.deletedDate = Date().time
                }
            }
            smsListResult.addAll(prevSmsList)
            for (sms in smsList) {
                var found = false
                for (prevSms in prevSmsList) {
                    if (sms == prevSms) {
                        found = true
                    }
                }
                if (!found) {
                    smsListResult.add(sms)
                }
            }
            smsDao.insertAllSmsList(smsListResult)
        }
    }

    override fun getPendingSmsList(callback: SmsDataSource.SmsListCallBack) {

    }

    companion object {
        private var INSTANCE: SmsDataSource? = null

        @JvmStatic
        fun getInstance(appExecutors: AppExecutors, smsDao: SmsDao): SmsDataSource {
            if (INSTANCE == null) {
                synchronized(SmsDatabaseDataSource::javaClass) {
                    INSTANCE = SmsDatabaseDataSource(appExecutors, smsDao)
                }
            }
            return INSTANCE!!
        }

        @VisibleForTesting
        fun clearInstance() {
            INSTANCE = null
        }
    }
}