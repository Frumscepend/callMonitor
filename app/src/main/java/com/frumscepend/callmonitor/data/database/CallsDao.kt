package com.frumscepend.callmonitor.data.database

import android.arch.persistence.room.*
import com.frumscepend.callmonitor.data.CallModel

/**
 * Data Access Object for products
 */

@Dao
interface CallsDao {

    /**
     * Getting all existing calls
     *
     * @return all calls
     */
    @Query("SELECT * FROM call")
    fun getAllCalls(): List<CallModel>

    /**
     * Getting all calls from date
     *
     * @return all calls from date
     */
    @Query("SELECT * FROM call WHERE date > :providedDate or deletedDate > :providedDate")
    fun getCallsFromDate(providedDate: Long): List<CallModel>

    /**
     * Getting call before date
     *
     * @return call before date
     */
    @Query("SELECT * FROM call WHERE ABS(:providedDate - (date + duration * 1000)) < 5000 ORDER BY id DESC LIMIT 1")
    fun getCallsBeforeDate(providedDate: Long): List<CallModel>

    /**
     * Getting call contains name
     *
     * @return call contains name
     */
    @Query("SELECT * FROM call WHERE :name = phoneNumber AND fileSent = 0 AND duration > 0 AND ABS(duration - :duration) <= 1 ORDER BY id ASC LIMIT 1")
    fun getCallsName(name: String, duration: Int): List<CallModel>

    /**
     * Creating set of new calls
     *
     * @param calls the calls list to be inserted
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllCalls(calls: List<CallModel>)

    /**
     * Update of call
     *
     * @param call the call to be updated
     */
    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateCall(call: CallModel)

}