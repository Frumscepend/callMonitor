package com.frumscepend.callmonitor.data

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import java.io.Serializable
import java.util.*

@Entity(tableName = "call")
class CallModel constructor(
        @PrimaryKey @ColumnInfo(name = "id") var id: String = UUID.randomUUID().toString(),
        @ColumnInfo(name = "uid") var uid: String? = null,
        @ColumnInfo(name = "phoneNumber") var phoneNumber: String = "",
        @ColumnInfo(name = "name") var name: String = "",
        @ColumnInfo(name = "type") var type: Int? = null,
        @ColumnInfo(name = "date") var date: Long = 0,
        @ColumnInfo(name = "duration") var duration: Int = 0,
        @ColumnInfo(name = "deleted") var deleted: Boolean = false,
        @ColumnInfo(name = "fileSent") var fileSent: Boolean = false,
        @ColumnInfo(name = "deletedDate") var deletedDate: Long? = null
): Serializable {
    override fun equals(other: Any?): Boolean {
        val call = other as CallModel
        return call.phoneNumber == this.phoneNumber
                && call.type == this.type
                && call.date == this.date
                && call.duration == this.duration
    }

    fun getLastDate(): Long{
        return if (deleted) {
            deletedDate!!
        } else {
            date
        }
    }
}