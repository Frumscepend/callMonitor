package com.frumscepend.callmonitor.data.database

import android.arch.persistence.room.*
import com.frumscepend.callmonitor.data.SmsModel

/**
 * Data Access Object for products
 */

@Dao
interface SmsDao {

    /**
     * Getting all existing products
     *
     * @return all products
     */
    @Query("SELECT * FROM sms")
    fun getAllSmsList(): List<SmsModel>

    /**
     * Getting all sms from date
     *
     * @return all sms from date
     */
    @Query("SELECT * FROM sms WHERE date > :providedDate or deletedDate > :providedDate")
    fun getCallsFromDate(providedDate: Long): List<SmsModel>

    /**
     * Creating set of new smsList
     *
     * @param smsList the smsList list to be inserted
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllSmsList(smsList: List<SmsModel>)

    /**
     * Update of sms
     *
     * @param call the sms to be updated
     */
    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateSms(sms: SmsModel)

}